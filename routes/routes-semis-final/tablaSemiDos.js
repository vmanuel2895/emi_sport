const {Router} = require('express');
const tablaSemiDos = require('../../models/Tablas-semis-final/semiDos');
const app = Router();

app.post('/enviar/tabla/Segunda/semi',(req, res)=>{

    const body = req.body;

    const respuesta = new tablaSemiDos({
        idAtleta : body.idAtleta,
        nivelAtletico: body.nivelAtletico,
        nivelTecnico: body.nivelTecnico,
        puntajeTotal: body.puntajeTotal
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"Datos Guardados Exitosamente a tabla Segunda semi final",
        respuesta
    })
});

app.get('/obtener/segundos/semi/finalistas',async (req,res)=>{

    const finalistas = await tablaSemiDos.find()
    .populate('idAtleta')

        // if(finalistas == "" || finalistas == null){
        //     res.status(404).json({
        //         ok:false,
        //         msj:"No hay  semi finalistas",
        //     })
        // }
        
        res.json({
         ok:true,
         msj:"Estos son los semi finalistas",
         finalistas
        });
    

});

module.exports = app;