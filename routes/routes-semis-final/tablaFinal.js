const {Router} = require('express');
const tablaFinal = require('../../models/Tablas-semis-final/final');
const app = Router();

app.post('/enviar/tabla/Finalistas',(req, res)=>{

    const body = req.body;

    const respuesta = new tablaFinal({
        idAtleta : body.idAtleta,
        nivelAtletico: body.nivelAtletico,
        nivelTecnico: body.nivelTecnico,
        puntajeTotal: body.puntajeTotal
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"Datos Guardados Exitosamente a tabla final",
        respuesta
    })
});

app.get('/obtener/finalistas',async (req,res)=>{

    const finalistas = await tablaFinal.find()
    .populate('idAtleta')

        // if(finalistas == "" || finalistas == null){
        //     res.status(404).json({
        //         ok:false,
        //         msj:"No hay finalistas",
        //     })
        // }
        
        res.json({
         ok:true,
         msj:"Estos son los finalistas",
         finalistas
        });
    

});

module.exports = app;