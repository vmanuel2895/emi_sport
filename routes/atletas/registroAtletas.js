const {Router, query} = require('express');
const atletanew = require('../../models/atletas/atletas');
const route = Router();
//////////////////////////////// REGISTRO DE ATLETLAS NUEVOS //////////////////////

route.post('/atleta/nuevo',(req,res)=>{

    let body = req.body;

     let registronewAtleta = new atletanew({
        nombre:body.nombre,
        apellidoPaterno:body.apellidoPaterno,
        apellidoMaterno:body.apellidoMaterno,
        curp:body.curp,
        edad:body.edad,
        sexo:body.sexo,
        estado:body.estado,
        modalidad:body.modalidad,
        integrantes:body.integrantes,
        colorCinta:body.colorCinta,
        categoria:body.categoria,
        equipo:body.equipo,
        division:body.division,
        presidente:body.presidente
        /* valores :body.valores,
        kataUno : body.kataUno,
        kataDos: body.kataDos,
        kataTres: body.kataTres,
        kataCuatro: body.kataCuatro,
        kataCinco : body.kataCinco,
        kataSeis: body.kataSeis,
        presidente: body.presidente,
        tatami:body.tatami */
 });//fin new presidente
    
registronewAtleta.save();

    res.json({
            ok:true,
            registronewAtleta
        })
});

/////////////////////// OBTENER TODOS LOS REGISTROS DE ATLETAS ////////////////////////
route.get('/obetner/atletas', async (req, res) => {

  const respuesta = await atletanew.find();
    res.json({
        ok:true,
        respuesta
    });
    
});





////////////////////////// BUSCAR ATLETA POR ID //////////////////////////////////////

route.get('/obtener/idarreglo/:id',async(req, res) =>{

    let id = req.params.id
    const respuesta = await atletanew.findById(id);
    res.json({
        ok:true,
        respuesta
    });

});

//////////////// actualiza las kata en copetencia ( no se repita la kata)////////////////

route.put('/actualiza/kata/realizada/:id', async (req, res)=>{
 let id = req.params.id;

 const existeID =  await atletanew.findById(id);
 
 if(!existeID){
     res.status(404).json({
       ok: false,
       msj:" No Existe registro con ese ID"
     });
 }

 const campos = req.body;
 delete campos.valores

 const respuestaUp = await atletanew.findByIdAndUpdate(id,campos,{new:true});

 res.json({
     ok:true,
     msj:"Datos Actualizados KATA",
     respuestaUp
 });

});

route.put('/actualizar/tatami/valores/:id',async(req,res)=>{
    let id = req.params.id
    const findTatami = await atletanew.findById(id);
    if (!findTatami) {
        respuesta.json({
            ok:false,
            msj:"No existe id"
        });
    }
    const body = req.body;
    delete body.valores,
    delete body.kataCinco,
    delete body.kataCuatro,
    delete body.kataTres,
    delete body.kataDos,
    delete body.kataUno,
    delete body.presidente
    const respuesta = await atletanew.findByIdAndUpdate(id, body, {new:true});
    respuesta.json({
        ok:true,
        msj:"Actualizo tatami",
        respuesta
    })
});

////////// DISTINC /////////////
route.get('/distinc/estado',async (req,res)=>{
     const esta = 'valores.estado'
     const body = req.body;
// const respuesta = await atletanew.find()
      const querys = {presidente :"n"}
const distinto = await atletanew.find()
const r = atletanew.distinct(esta)
res.json({
    ok:true,
    msj:"Muestra los estado son repetidos",
   r
})
});

route.put('/actualiza/atleta/:id', async (req, res)=>{
    let id = req.params.id;
    const campos = req.body;
    const respuestaUp = await atletanew.findByIdAndUpdate(id,campos,{new:true});
   
    res.json({
        ok:true,
        msj:"Datos Actualizados del Atleta",
        respuestaUp
    });
   
});

route.delete('/eliminar/atleta/:id', async(req, res) => {
    let id = req.params.id;
    const respuesta = await atletanew.findByIdAndDelete(id);
    res.json({
        ok:true,
        msj:"Dato eliminado del Atleta",
        respuesta
    });
});


module.exports = route;