const {Router, response} = require('express');
const compe = require('../../models/registroCompe/registroCompe');
const route = Router();

route.post('/registro/competencias/cate',async (req,res)=>{
    
    let body = req.body;
    let registronewCompe = new compe({
        categoria:body.categoria,
        genero:body.genero,
        grado:body.grado,
        vueltas:body.vueltas,
        equipoAKA:body.equipoAKA,
        equipoAO:body.equipoAO
    });
        
    registronewCompe.save();
    res.json({
        ok:true,
        registronewCompe
    });
});

route.post('/obtener/comptenecias/cate',async (req,res)=>{

    let body = req.body
    const respuesta = await compe.find({ categoria: body.categoria, genero:body.genero,grado:body.grado });
    res.json({
        ok:true,
        respuesta
    });
});

route.put('/actualizar/competencias/:id', async (req,res)=>{
    let id = req.params.id;
    const campos = req.body;
    const respuestaUp = await compe.findByIdAndUpdate(id,campos,{new:true});
   
    res.json({
        ok:true,
        msj:"Datos Actualizados",
        respuestaUp
    });
})

module.exports = route;