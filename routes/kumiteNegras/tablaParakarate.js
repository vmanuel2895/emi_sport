const {Router}= require('express');
const categoriaTabla = require('../../models/negrasKumite/parakarate');
const app = Router();

app.get('/obtener/categorias/kata/parakarate', async (req,res)=>{
    const edades = await categoriaTabla.find();
    
    res.json({
     ok:true,
     msj:"Estos son los categorias",
     edades
    });
});

app.post('/agregar/nueva/categoria/kata/parakarate',(req,res)=>{
  const body = req.body;
    const respuesta = new categoriaTabla({
        categoria : body.categoria
    });

    respuesta.save();
    res.json({
        ok:true,
        msj:"categoria enviada",
        respuesta
    })
});

module.exports = app;