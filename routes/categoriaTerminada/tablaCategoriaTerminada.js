const {Router} = require('express');
const CategoriaTerminada = require('../../models/categoriasTerminadas/categoriasTerminadas')
const app=Router();

app.post('/enviar/categoria/terminada', (req,res) =>{
    const body = req.body
    const respuesta = new CategoriaTerminada({
        categoria:body.categoria,
        division :body.division,
        rango : body.rango,
        genero: body.genero
    });
    respuesta.save();
    res.json({
        ok:true,
        msj:'Categoria Terminada guardada con exito',
        respuesta
    });
});

app.get('/obtener/categorias/terminadas', async(req,res)=>{
    const respuesta = await CategoriaTerminada.find();
    res.json({
        ok:true,
        msj:'Estas son las categorias terminadas',
        respuesta
    })
})
module.exports = app;