const express = require('express');
const app = express();

app.use(require('./presidentes/presi'));

//####################### Pagina Femeka Isis #############################
app.use(require('./atletas/registroAtletas'));
app.use(require('./entrenadores/routeEntrenadores'));
app.use(require('./jueces/routeJuez'));
app.use(require('./examinador/routeExaminador'));
app.use(require('./login'));
app.use(require('./boucherImg/routeBoucher'));
//########################################################################

//######################### FEMEKA KATA y Commite ##############################
app.use(require('./calificaciones/puntajes'));
app.use(require('./routes-semis-final/tablaSemiUno'));
app.use(require('./routes-semis-final/tablaSemiDos'));
app.use(require('./routes-semis-final/tablaFinal'));
app.use(require('./routes-semis-final/tablaWinners'));
app.use(require('./routes-semis-final/tablaResultados'));
app.use(require('./katas/tablaKatas'));
app.use(require('./categorias/tablaCategorias'));
app.use(require('./tatamis/tablaTatamis'));
app.use(require('./divisiones/tablaDivisiones'));
app.use(require('./categoriaTerminada/tablaCategoriaTerminada'));
app.use(require('./categoriasKyu/gradosKyu'));
app.use(require('./kumiteNegras/tablaKumiteNegra'));


// ########################### VIRTUAL EDADES ###################################

app.use(require('./categoriasKyu/hombresKyu'));
app.use(require('./categoriasKyu/mujeresKyu'));

app.use(require('./categorias/negrasOctubre/hombres'));
app.use(require('./categorias/negrasOctubre/mujeres'));

//################################## new valores

app.use(require('./kumiteNegras/tablaKumiteNegra'));
app.use(require('./kumiteNegras/tablaMujueres'));
app.use(require('./kumiteNegras/tablaParakarate'));

app.use(require('./registroCompe/registroCompe'))

module.exports = app;