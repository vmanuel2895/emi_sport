const {Router}= require('express');
const calificacion = require('../../models/calificaciones/marcacion');
const app = Router();

////////////////////// envia y guarda puntajes de un atleta KATA //////////////////////

app.post('/guardar/puntaje/atleta/kata',(req, res)=>{

    const body = req.body;

    const respuesta = new calificacion({
    idAtleta : body.idAtleta,
    nivelAtletico: body.nivelAtletico,
    nivelTecnico: body.nivelTecnico,
    puntajeTotal: body.puntajeTotal
    });

    respuesta.save();
    res.json({
    ok:true,
     msj:'Calificaciones Enviadas Con Exito',
     respuesta
    });

});

///////////////////////////// obetiene el id del atleta y sus porcentajes ///////////////

app.get('/obtener/puntaje/atleta/kata/:id', async (req,res)=>{

    const id = req.params.id
const body =  req.body;
   const respuesta=  await calificacion.findById(id)
      .populate('idAtleta');

    res.json({
    ok:true,
   msj:"Encontre Atleta y Porcentaje",
     respuesta,
    });
});


//////////////////// actualiza el porcenytaje de atleta en la siguientes rondas ///////////
app.put('/actualizar/puntaje/:id', async (req,res)=>{

const id = req.params.id;
try {
 const existe = await calificacion.findById(id);

 if(!existe){
     res.json({
         ok:false,
         msj:"No existe un id en el documento"
     });
 }

 const campos = req.body;

 const respuesta = await calificacion.findByIdAndUpdate(id,campos,{new:true});
 res.json({
     ok:true,
     msj:"Se actualizo porcentajes exitosamente",
     respuesta
 });
    
} catch (error) {
    console.log(error);
    res.status(500).json({
     ok:false,
     msj:"Error al actualizar porcentaje"
    });
}
});

module.exports = app;