const { Schema, model } = require('mongoose');

const JuezShema = Schema({
    apellidoMaterno: {type : String,default:""},
    apellidoPaterno: {type : String,default:""},
    curp: {type : String,default:""},
    estado: {type : String,default:""},
    estilo: {type : String,default:""},
    fechaNacimiento: {type : String,default:""},
    gradoCN: {type : String,default:""},
    licencia: {type : String,default:""},
    nombre: {type : String,default:""},
    padecimientoCronico: {type : String,default:""},
    sexo: {type : String,default:""},
    tipoSangre: {type : String,default:""},
    presidente:{type : String,default:""}
/* camposJueces : {type : Array} */
});
module.exports = model ('Jueces', JuezShema);