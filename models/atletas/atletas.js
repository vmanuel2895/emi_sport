const { Schema, model } = require('mongoose');

const AtletaShema = Schema({
// nombre :{
//     type :String,
// },
// apellidoPaterno :{
//     type :String, 
// },
// apellidoMaterno :{
//     type :String, 
// },
// sexo :{
//     type :String,
// },
// estado :{
//     type :String,
// },
// modalidad :{
//     type :String,
// },
// integrantes :{
//     type :String,
// },
// categoria :{
//     type :String,
// }

nombre:{type : String,default:""},
apellidoPaterno:{type : String,default:""},
apellidoMaterno:{type : String,default:""},
curp:{type : String,default:""},
edad:{type : String,default:""},
sexo:{type : String,default:""},
estado:{type : String,default:""},
modalidad:{type : String,default:""},
integrantes:{type : String,default:""},
colorCinta:{type : String,default:""},
categoria:{type : String,default:""},
equipo:{type : String,default:""},
division:{type : String,default:""},
presidente:{type : String,default:""}


/* valores :{type : Array},
kataUno: {type : String,default:""},
kataDos: {type : String,default:""},
kataTres: {type : String,default:""},
kataCuatro: {type : String,default:""},
kataCinco: {type : String,default:""},
kataSeis: {type : String,default:""},
presidente: {type: String},
tatami: {type:String, default:""} */


});
module.exports = model ('Atletas', AtletaShema);