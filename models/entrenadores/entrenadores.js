const { Schema, model } = require('mongoose');

const EntrenaShema = Schema({
// nombre :{
//     type :String,
// },
// apellidoPaterno :{
//     type :String, 
// },
// apellidoMaterno :{
//     type :String, 
// },
// sexo :{
//     type :String,
// },
// estado :{
//     type :String,
// },
// curp :{
//     type :String,
// },
// gradoCN :{
//     type :String,
// },
// estilo :{
//     type :String,
// },
// fechaNacimiento :{
//     type :String,
// }

apellidoMaterno:{type : String,default:""},
apellidoPaterno:{type : String,default:""},
curp:{type : String,default:""},
estado:{type : String,default:""},
estilo:{type : String,default:""},
fechaNacimiento:{type : String,default:""},
gradoCN:{type : String,default:""},
nombre:{type : String,default:""},
sexo:{type : String,default:""},
presidente:{type : String,default:""}

/* campos: {type : Array} */

});
module.exports = model ('Entrenadores', EntrenaShema);