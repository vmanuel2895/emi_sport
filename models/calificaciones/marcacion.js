const {Schema, model} = require('mongoose');

const caliSchema = new Schema({
    idAtleta : {type: Schema.Types.ObjectId, ref:'Atletas'},
    nivelAtletico: {type : Array},
    nivelTecnico: { type: Array},
    puntajeTotal: {type : Number}

});

module.exports = model ('Calificaciones',caliSchema);