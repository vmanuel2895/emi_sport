const { Schema, model} = require('mongoose');

const katasSchema = Schema({

    numero:{ type: String},
    nombre: {type: String}

});

module.exports = model('katas', katasSchema);