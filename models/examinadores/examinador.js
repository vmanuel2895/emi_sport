const { Schema, model } = require('mongoose');

const exaShema = Schema({
    apellidoMaterno: {type : String,default:""},
    apellidoPaterno: {type : String,default:""},
    curp: {type : String,default:""},
    estado: {type : String,default:""},
    estilo: {type : String,default:""},
    fechaNacimiento: {type : String,default:""},
    gradoCN: {type : String,default:""},
    nombre: {type : String,default:""},
    sexo: {type : String,default:""},
    presidente:{type : String,default:""}
/* camposExaminadores : {type: Array} */

});
module.exports = model ('Examinadores', exaShema);