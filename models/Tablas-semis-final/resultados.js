const { Schema, model} = require('mongoose');

const resultadoSchema = Schema({
    idAtleta : {type: Schema.Types.ObjectId, ref:'Atletas'},
    nivelAtletico: {type : Array},
    nivelTecnico: { type: Array},
    puntajeTotal: {type : Number},
    tatami: {type: String},
    equipo: {type: String},
    kata:{type:String},
    ronda: {type:String}
});

 module.exports= model('Resultados',resultadoSchema);