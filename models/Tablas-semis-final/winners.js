const { Schema, model} = require('mongoose');

const winnerSchema = Schema({
    idAtleta : {type: Schema.Types.ObjectId, ref:'Atletas'},
    puntajeTotal: {type : Number},
    lugar: {type: String},
    rango :{type:String},
    division:{type: String}
});

 module.exports= model('Winners',winnerSchema);