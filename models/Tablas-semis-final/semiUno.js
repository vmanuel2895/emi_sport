const {Schema, model} = require('mongoose');


const SemiUnoSchema = Schema({
    idAtleta : {type: Schema.Types.ObjectId, ref:'Atletas'},
    nivelAtletico: {type : Array},
    nivelTecnico: { type: Array},
    puntajeTotal: {type : Number}
});

module.exports = model ('Semiuno',SemiUnoSchema);