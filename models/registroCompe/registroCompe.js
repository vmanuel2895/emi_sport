const { Schema, model } = require('mongoose');

const CompeShema = Schema({
    categoria:{type : String,default:""},
    genero:{type : String,default:""},
    grado:{type : String,default:""},
    vueltas:{type: Number,default:1},
    equipoAKA:{type : Array},
    equipoAO:{type : Array}
});
module.exports = model ('Competencias', CompeShema);