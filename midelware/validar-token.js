const jwt = require('jsonwebtoken');

const validarJWT = (req,res,next)=>{

    const token = req.header('x-token');
    console.log(token);

    if(!token){
        return res.status(401).json({
       ok: false,
       msg:"No hay token en la peticion"
        })
    }

    try {
        
        const {uid} = jwt.verify(token,'F988hgsggsMwecrfvrvK488dcrr');
        console.log(uid);
        req.uid =uid;
        next();
    } catch (error) {
        res.status(401).json({
            ok: false,
            mng:"Token no valido"
        })
    }

   
}

module.exports ={
    validarJWT
}