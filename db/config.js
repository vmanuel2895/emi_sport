const mongoose = require('mongoose');
const port = process.env.PORT || 3000;

const dbConnection = async() =>{
    try {
        // mongodb+srv:dbaFEMEKA:FMKdb@cloudfemeka.y2tpq.mongodb.net/FMK?authSource=admin&replicaSet=atlas-akpu6z-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true
       await mongoose.connect(' mongodb+srv://dbaFEMEKA:FMKdb@cloudfemeka.y2tpq.mongodb.net/virtual?retryWrites=true&w=majority',
    // await mongoose.connect('mongodb+srv:dbaFEMEKA:FMKdb@cloudfemeka.y2tpq.mongodb.net/FMK?authSource=admin&replicaSet=atlas-akpu6z-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true',
         {useNewUrlParser: true,
             useUnifiedTopology: true,
             useFindAndModify: false});
    console.log('DB FMK ONLINE');
    } catch (error) {
        throw new Error ('Error a la hora de iniciar DB')
    }
}

module.exports = {
    dbConnection
}